﻿namespace Core
{
    public interface IEnterState
    {
        void Enter();
    }
}
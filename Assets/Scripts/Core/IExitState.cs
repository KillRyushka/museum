﻿namespace Core
{
    internal interface IExitState
    {
        void Exit();
    }
}
﻿using UnityEngine;

namespace Core
{
    public class SleepState : IEnterState, IExitState
    {
        private IOpenPanel _panel;

        public SleepState(IOpenPanel panel)
        {
            _panel = panel;
        }
        public void Enter()
        {
            _panel.Open();
        }

        public void Exit()
        {
        }
    }

    public class OpenSleepPanel : IOpenPanel
    {
        public void Open()
        {
            Debug.Log("opened sleep panel!");
        }
    }
}
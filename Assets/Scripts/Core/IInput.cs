﻿using UnityEngine;

namespace Core
{
    public interface IInput
    {
        Vector2 GetMoveDirection();
        string GetPressedKey();
    }
}
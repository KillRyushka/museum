﻿using UnityEngine;

namespace Core
{
    public class Gameplay:IGamePlay
    {
        private PlayerMovement _playerMovement;
        private PlayerInteract _playerInteract;
        private Transform _player;
        private float _moveSpeed;

        public Gameplay(PlayerMovement playerMovement, PlayerInteract playerInteract,Transform player,float moveSpeed)
        {
            _playerMovement = playerMovement;
            _playerInteract = playerInteract;
            _player = player;
            _moveSpeed = moveSpeed;
        }

        public void Release()
        {
            _playerMovement.Move(_player,_moveSpeed);
            _playerInteract.Interact();
        }
    }
}
﻿using UnityEngine;
namespace Data
{
    [CreateAssetMenu(menuName = "StaticData/new UIPanelStaticData", fileName = "UIPanelStaticData")]
    public class UIStaticPanelsData : ScriptableObject
    {
        public UIPanelTypes Types;
        public GameObject prefab;
    }

}
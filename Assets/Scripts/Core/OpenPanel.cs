﻿using UnityEngine;

namespace Core
{
    public class OpenPanel : IOpenPanel
    {
        public void Open()
        {
            Debug.Log("Panel opened!");
        }
    }
}
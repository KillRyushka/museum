﻿namespace Core
{
    internal interface IInteract
    {
        void Interact();
    }
}
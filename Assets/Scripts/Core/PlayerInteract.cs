﻿using UnityEngine;
namespace Core
{
    public class PlayerInteract :  IInteract
    {
        private string _pressedButton;
        private IInput _input;

        public PlayerInteract(IInput input)
        {
            _input = input;
        }

      /*  private void Awake()
        {
            _input = new InputService();
        }

        private void Update()
        {
            Interact();
        }*/
        public void Interact()
        {
            _pressedButton = _input.GetPressedKey();
            if(_pressedButton!="")
                Debug.Log(_pressedButton);
            _pressedButton = "";
        }
    }
}
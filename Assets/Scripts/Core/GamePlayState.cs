﻿namespace Core
{
    public class GamePlayState : IEnterState, IExitState
    {
        private IGamePlay _gameplay;

        public GamePlayState(IGamePlay gameplay)
        {
            _gameplay = gameplay;
        }
        public void Enter()
        {
            _gameplay.Release();//tolko nyshno delati eto v update
        }

        public void Exit()
        {
        }
    }
}
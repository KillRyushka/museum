﻿using UnityEngine;
namespace Core
{
    public class PlayerMovement:IMove
    {
        private Vector2 _direction;
        private IInput _input;
        public PlayerMovement(IInput input)
        {
            _input = input;
        }
/*
        private void Awake()
        {
            _input = new InputService();
            _player = transform;
        }
        private void Update()
        {
            Move(_player);
        }*/
        public void Move(Transform player,float moveSpeed)
        {
            _direction= _input.GetMoveDirection();
            player.Translate(_direction*moveSpeed*Time.deltaTime);
        }
    }
}
﻿namespace Core
{
    public interface IOpenPanel
    {
        void Open();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Core
{
    public class InputService : IInput
    {
        private Vector2 _direction;
        private string _inputKey;

        public Vector2 GetMoveDirection()
        {
            _direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized;
            return _direction;
        }

        public string GetPressedKey()
        {
            _inputKey = Input.inputString;
            return _inputKey;
        }
    }
}
﻿namespace Core
{
    public class WaitForInputState : IEnterState,IExitState
    {
        private IOpenPanel _panel;
        public WaitForInputState(IOpenPanel panel)
        {
            _panel = panel;
        }
        public void Enter()
        {
            _panel.Open();
        }

        public void Exit()
        {
        }
    }
}
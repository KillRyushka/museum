﻿using UnityEngine;
namespace Core
{
    public class Bootstrapper :MonoBehaviour
    {
        [SerializeField] private Transform player;
        [SerializeField] private float moveSpeed;
        private InputService input=new InputService();
        private Gameplay gameplay;
        private GamePlayState gamePlayState ;
        private void Awake()
        {
            gameplay = new Gameplay(new PlayerMovement(input), new PlayerInteract(input), player,moveSpeed);
            gamePlayState = new GamePlayState(gameplay);
        }
        private void Update()
        {
            gamePlayState.Enter();
        }
    }
}
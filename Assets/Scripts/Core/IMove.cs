﻿using UnityEngine;

namespace Core
{
    public interface IMove
    {
        void Move(Transform transform,float moveSpeed);
    }
}